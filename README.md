# Jundoo Tana display

A display used in an exhibition.

This project is optimized to run on a raspberry pi connected to an
Arduino micro pro.

## Project structure 

```
1     |- docs
2     |- enclosure
3     |- firmware
4     |- hardware
4.1   |- |-- pcbs
4.1.1 |- |-- | - EAGLE
5     |- tools
5.1   |- |-- scan_i2c
6.    |- www
_______________________________________________________

1. Docs

   Find here all Datasheets and all relative documents

2. Enclosure

   Stl files for 3D printing

3. Firmware

   This folder contains the firmares uploaded on used microcontroler

4. Hardware

   All files related to the hardware of the project (pcbs and protoboards)

   * 4.1 pcbs

		Contains pcb designs

5. tools

   This folder is used to store all the tests on different components and dev boards.

6. www
   the folder contains source code of the web app launched in kiosk mode

```


## Getting Started

These instructions will get you a copy of the project up and running on your local machine
for development and testing purposes. See deployment for notes on how to deploy the project
on a live system.

### Prerequisites

Things to be installed for developpement :

```
- node js
- webpack
- arduino ide
```

## Built With

* [Arduino IDE](https://www.arduino.cc/en/Main/Software) - Arduino IDE for arduino developpement
* [Webpack](https://webpack.js.org/) - Build system running on node js
