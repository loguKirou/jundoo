
#include <ArduinoJson.h>

const int  buttonPinA = 2;
const int  buttonPinB = 3;
const int  buttonPinC = 4;
const int  buttonPinD = 5;
const int  buttonPinE = 6;
const int  buttonPinF = 7;
const int  buttonPinG = 8;
const int  buttonPinH = 9;

// intitial wood type
char tanaWoodType = "A";
int tanaWidth = 0;
int tanaHeight = 4;

int previousPosition = 0;

// Variables will change:
int buttonPushCounter = 0;   // counter for the number of button presses
int buttonState[] = {0,0,0,0,0,0,0,0};         // current state of the button
char buttonid[] = {'A','B','C','D','E','F','G','H'};         // current state of the button
int lastButtonState [] = {0,0,0,0,0,0,0,0};     // previous state of the button


void setup() {
  
  // start serial connection
  Serial.begin(9600);
  // Set pin to imputs and activate pullup
  pinMode(buttonPinA, INPUT_PULLUP);
  pinMode(buttonPinB, INPUT_PULLUP);
  pinMode(buttonPinC, INPUT_PULLUP);
  pinMode(buttonPinD, INPUT_PULLUP);
  pinMode(buttonPinE, INPUT_PULLUP);
  pinMode(buttonPinF, INPUT_PULLUP);
  pinMode(buttonPinG, INPUT_PULLUP);
  pinMode(buttonPinH, INPUT_PULLUP);

}

void loop() {
  // read the pushbutton input pins
  buttonState [0]= digitalRead(buttonPinA);
  buttonState [1]= digitalRead(buttonPinB);
  buttonState [2]= digitalRead(buttonPinC);
  buttonState [3]= digitalRead(buttonPinD);
  buttonState [4]= digitalRead(buttonPinE);
  buttonState [5]= digitalRead(buttonPinF);
  buttonState [6]= digitalRead(buttonPinG);
  buttonState [7]= digitalRead(buttonPinH);
  // read slider position
  int Potaposition = analogRead(3); 
  int i;
  for (i = 0; i < 9; i = i + 1) { 
    // compare the buttonState to its previous state
    if (buttonState[i] != lastButtonState[i]) {
      // if the state has changed, increment the counter
      if (buttonState[i] == HIGH) {
        // if the current state is HIGH then the button went from off to on:
        buttonPushCounter++;
        if (buttonid[i] == buttonid[6])
        {
          if (tanaHeight < 7) tanaHeight ++;
        }
        else if (buttonid[i] == buttonid[7])
        {
          if (tanaHeight > 4) tanaHeight --;
        }
        else {
          tanaWoodType = buttonid[i];
        }
        // Delay a little bit to avoid bouncing
        delay(50);
      }
    }
    // save the current state as the last state, for next time through the loop
    lastButtonState[i] = buttonState[i];
  };
  // Convert slider position to display size
  if (((Potaposition/16) + 120) % 2) { 
    tanaWidth = (Potaposition/16) + 120 + 1; 
  } else {
    tanaWidth = (Potaposition/16) + 120;
  }
  // Limit size
  if (tanaWidth > 180) {tanaWidth = 180;}
  if (tanaWidth < 120) {tanaWidth = 120;}
  // Write to serial port
  String buf;
  buf += "{";
  buf += "\"woodType\":\""; buf += tanaWoodType; buf += "\",";
  buf += "\"width\":"; buf += tanaWidth; buf += ",";
  buf += "\"height\":"; buf += tanaHeight;
  buf += "}";
  Serial.println(buf);
}


