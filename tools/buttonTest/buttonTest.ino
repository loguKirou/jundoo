const int  buttonPinA = 2;
const int  buttonPinB = 3;
const int  buttonPinC = 4;
const int  buttonPinD = 5;
const int  buttonPinE = 6;
const int  buttonPinF = 7;
const int  buttonPinG = 8;
const int  buttonPinH = 9;

int previousPosition = 0;

// Variables will change:
int buttonPushCounter = 0;   // counter for the number of button presses
int buttonState[] = {0,0,0,0,0,0,0,0};         // current state of the button
char buttonid[] = {'A','B','C','D','E','F','G','H'};         // current state of the button
int lastButtonState [] = {0,0,0,0,0,0,0,0};     // previous state of the button


void setup() {
  
  //start serial connection
  Serial.begin(9600);
  pinMode(buttonPinA, INPUT_PULLUP);
  pinMode(buttonPinB, INPUT_PULLUP);
  pinMode(buttonPinC, INPUT_PULLUP);
  pinMode(buttonPinD, INPUT_PULLUP);
  pinMode(buttonPinE, INPUT_PULLUP);
  pinMode(buttonPinF, INPUT_PULLUP);
  pinMode(buttonPinG, INPUT_PULLUP);
  pinMode(buttonPinH, INPUT_PULLUP);

}

void loop() {
  // read the pushbutton input pin:
  buttonState [0]= digitalRead(buttonPinA);
  buttonState [1]= digitalRead(buttonPinB);
  buttonState [2]= digitalRead(buttonPinC);
  buttonState [3]= digitalRead(buttonPinD);
  buttonState [4]= digitalRead(buttonPinE);
  buttonState [5]= digitalRead(buttonPinF);
  buttonState [6]= digitalRead(buttonPinG);
  buttonState [7]= digitalRead(buttonPinH);
  
  int Potaposition = (analogRead(3) * 100)/1023; 
  int i;
  for (i = 0; i < 8; i = i + 1) { 
    // compare the buttonState to its previous state
    if (buttonState[i] != lastButtonState[i]) {
      // if the state has changed, increment the counter
      if (buttonState[i] == HIGH) {
        // if the current state is HIGH then the button went from off to on:
        buttonPushCounter++;
        Serial.println(buttonid[i]);
      } else {
      }
      // Delay a little bit to avoid bouncing
      
      delay(50);
    }
    // save the current state as the last state, for next time through the loop
    lastButtonState[i] = buttonState[i];
  }
  // Serial.println( Potaposition );
  if (Potaposition > previousPosition) {
    Serial.println( "GROW" );
    delay(100);
  }
  else if (Potaposition < previousPosition)  {
    Serial.println( "SHRINK" );
    delay(100);
  }
  previousPosition = Potaposition;
}


