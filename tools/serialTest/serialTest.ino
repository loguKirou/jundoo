int count = 0;
unsigned long previousMillis = 0;     
const long interval = 20000;  

void setup() {
  // put your setup code here, to run once:
 Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
    previousMillis = currentMillis;
    Serial.println("CHANGE_WOOD");
  }
}
