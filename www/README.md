# Display

This is the UI part of the project. It hosts the webapp which is displayed on
the device's screen.

# Setting up the environment

## Environment
Make sure you have [Node.js](http://nodejs.org/)

## Install the dependencies

    npm install

* Adding new packages : `npm install package-name --save` (will add the
dependency to package.json)
* Adding packages used only on development : `npm install package-name --save-dev`


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

Open browser to [http://localhost:8080](http://localhost:8080)

## Infos

#### Vue JS
- [docs for vue-loader](http://vuejs.github.io/vue-loader)
- [docs for vue-router](http://router.vuejs.org/en/)


#### Build system

- [vue.js webpack guide](http://vuejs-templates.github.io/webpack/)


