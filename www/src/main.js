/**
 * Import Global Style (.css/.scss)
 */
import './assets/scss/index.scss' // Customize UI

/**
 * Import Dependency
 */
import Vue from 'vue'
import socketio from 'socket.io-client'
import IdleVue from 'idle-vue'
import VueSocketIO from 'vue-socket.io'
import VueRamda from 'vue-ramda'
import router from './router'
import store from './store'
import head from 'vue-head'
import 'es6-promise/auto'
import { sync } from 'vuex-router-sync'
import i18n from './locales'
const idleTimeout = 3 * 60 * 1000
/**
 * Import Component (.vue)
 */
import App from './App.vue'

export const SocketInstance = socketio('http://localhost:3000')

/**
 * Global Config
 */
Vue.config.productionTip = false
const EventBus = new Vue()
Object.defineProperties(Vue.prototype, {
	$bus: {
		get: function() {
			return EventBus
		}
	}
})
Vue.use(head)
Vue.use(VueSocketIO, SocketInstance, store)
Vue.use(VueRamda)
Vue.use(IdleVue, {
  eventEmitter: EventBus,
  idleTime: idleTimeout
})

sync(store, router)
const lang = store.state.language
if (lang) {
	i18n.locale = lang
}

/* eslint-disable no-new */
new Vue({
	el: '#app',
	router,
	store: store,
	i18n,
	template: '<App/>',
	components: { App }
})
