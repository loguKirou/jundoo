/**
 * Import Dependency
 */
import Vue from 'vue'
import Router from 'vue-router'

/**
 * Import Component (.vue)
 * For Async Component Syntax
 * const X = () => import('@/pages/xxx/xxx.vue')
 */
import Dashboard from '@/pages/dashboard'

/**
 * Config
 */
Vue.use(Router)

/**
 * Declare Variable
 */

const router = new Router({
	mode: 'history',
	routes: [
		{
			path: '/',
      redirect: '/dashboard'
		},
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard,
      meta: {
        requiresAuth: false
      }
    }
	]
})

router.afterEach((to, from) => {

})

export default router
