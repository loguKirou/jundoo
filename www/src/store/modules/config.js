const state = {
  wood: '',
  width: '',
  steps: ''
}
const mutations = {
  SOCKET_CONNECT(state) {
    console.log('io connected')
  },

  SOCKET_DISCONNECT(state) {
    console.log('io disconnected')
  },

  SOCKET_SENSORS(state, sensors) {
    var s = JSON.parse(sensors[0])
    var w = ''
    switch (s.woodType) {
      case 'A':
        w = 'Hetrerouge'
        break
      case 'B':
        w = 'Hetremarron'
        break
      case 'C':
        w = 'Hetre'
        break
      case 'D':
        w = 'Chenerouge'
        break
      case 'E':
        w = 'Chenemarron'
        break
      default:
        w = 'Chene'
    }
    state.wood = w
    state.width = s.width
    state.steps = s.height
  }
}

/**
 * Export
 */
export default {
  state,
  mutations
}
